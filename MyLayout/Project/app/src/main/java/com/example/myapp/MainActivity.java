package com.example.myapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import java.time.Instant;

public class MainActivity extends AppCompatActivity {
    ImageButton a1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        a1=(ImageButton)findViewById(R.id.imageButton);
        a1.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent a1 = new Intent(MainActivity.this,page1.class);
          startActivity(a1);
            }
        });
    }
}