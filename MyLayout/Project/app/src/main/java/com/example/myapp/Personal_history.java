package com.example.myapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class Personal_history extends AppCompatActivity {
    Button a3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_history);
        a3=(Button) findViewById(R.id.button6);
        a3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a3 = new Intent(Personal_history.this,page1.class);
                startActivity(a3);
            }
        });

    }
}