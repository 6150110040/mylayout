package com.example.myapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class page1 extends AppCompatActivity {
    Button a2,a4,a6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page1);
        a2=(Button) findViewById(R.id.button);
        a2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a2 = new Intent(page1.this,Personal_history.class);
                startActivity(a2);

            }
        });
        a4=(Button) findViewById(R.id.button4);
        a4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a4 = new Intent(page1.this,Favorite_song.class);
                startActivity(a4);
            }
        });
        a6=(Button) findViewById(R.id.button5);
        a6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a6 = new Intent(page1.this,Contact_cannel.class);
                startActivity(a6);
            }
        });
    }
}