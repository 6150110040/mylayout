package com.example.myapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Favorite_song extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_song);

    }

    public void imageButton2(View view) {
        Intent imageButton2 = new Intent(Intent.ACTION_VIEW,Uri.parse("https://youtu.be/PBZFGmJKgAo"));
        startActivity(imageButton2);

    }

    public void imageButton8(View view) {
        Intent imageButton8 = new Intent(Intent.ACTION_VIEW,Uri.parse("https://youtu.be/8rZ5gE0LVew"));
        startActivity(imageButton8);
    }

    public void imageButton9(View view) {
        Intent imageButton9 = new Intent(Intent.ACTION_VIEW,Uri.parse("https://youtu.be/z1I6wLw2vm4"));
        startActivity(imageButton9);
    }

    public void imageButton12(View view) {
        Intent imageButton12 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://youtu.be/7iSia7rb1PY"));
        startActivity(imageButton12);
    }
    public void button7(View view) {
        Intent button7 = new Intent(Favorite_song.this,page1.class);
        startActivity(button7);
    }
}